import {BasePaginatedDataTableSource} from "./base-paginated-data-table-source";


export class EmptyPaginatedDataTableSource<T> extends BasePaginatedDataTableSource<T>
{
	public reload(): void {}
}
