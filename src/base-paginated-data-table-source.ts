import {MatTableDataSource} from "@angular/material/table";


export abstract class BasePaginatedDataTableSource<T> extends MatTableDataSource<T>
{
	public abstract reload(): void;
}
