import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

import {BasePaginatedDataTableSource} from './base-paginated-data-table-source';
import {PaginatedData} from './paginated-data';


export class PaginatedDataTableSource<T> extends BasePaginatedDataTableSource<T>
{
	public readonly loading: Observable<boolean>;

	private dataSubject = new BehaviorSubject<Array<T>>([]);

	private loadingSubject = new BehaviorSubject<boolean>(false);

	private paginatorSubscription: Subscription|undefined;

	private sortSubscription: Subscription|undefined;

	constructor(
		paginator: MatPaginator,
		sort: MatSort,
		private query: (pageIndex: number, itemsPerPage: number, sort: Sort) => Promise<PaginatedData<T>>,
	) {
		super([]);

		this.paginator = paginator;
		this.sort = sort;
		this.loading = this.loadingSubject.asObservable();
	}

	public get data(): Array<T>
	{
		return this.dataSubject.getValue();
	}
	public set data(data: Array<T>)
	{
		this.dataSubject.next(data);
	}

	public connect(): BehaviorSubject<Array<T>>
	{
		this.paginatorSubscription = this.paginator.page.subscribe(async (page: PageEvent) => {
			await this.loadData(page.pageIndex, page.pageSize, {
				active: this.sort.active,
				direction: this.sort.direction,
			});
		});

		this.sortSubscription = this.sort.sortChange.subscribe(async (sort: Sort) => {
			this.paginator.pageIndex = 0;
			await this.loadData(this.paginator.pageIndex, this.paginator.pageSize, sort);
		});

		this.reload();

		return this.dataSubject;
	}

	public disconnect(): void
	{
		this.dataSubject.complete();
		this.loadingSubject.complete();

		if (typeof this.paginatorSubscription !== 'undefined') {
			this.paginatorSubscription.unsubscribe();
		}

		if (typeof this.sortSubscription !== 'undefined') {
			this.sortSubscription.unsubscribe();
		}
	}

	public reload(): void
	{
		this.loadData(this.paginator.pageIndex, this.paginator.pageSize, {
			active: this.sort.active,
			direction: this.sort.direction,
		}).then();
	}

	private async loadData(pageIndex: number, pageSize: number, sort: Sort): Promise<void>
	{
		this.loadingSubject.next(true);

		let items: Array<T> = [];

		try {
			const data = await this.query(pageIndex, pageSize, sort);
			this.paginator.length = data.length;
			this.paginator.pageSize = data.pageSize;
			items = data.data;
		} catch (e) {}

		this.loadingSubject.next(false);
		this.data = items;
	}
}
