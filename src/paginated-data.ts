export class PaginatedData<T>
{
	constructor(
		public readonly data: Array<T>,
		public readonly length: number,
		public readonly pageIndex: number,
		public readonly pageSize: number,
	) {}
}
