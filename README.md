# DKX/Angular/MatPaginatedTableSource

Paginated data source for material table

## Installation

```bash
$ npm install --add @dkx/mat-paginated-table-source
```

## Usage

**list.component.html:**

```html
<table *ngIf="dataSource" mat-table matSort [dataSource]="dataSource">
    <!-- todo -->
</table>

<mat-paginator></mat-paginator>
```

**list.component.ts:**

```typescript
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, Sort} from '@angular/material';
import {PaginatedDataTableSource} from '@dkx/mat-paginated-table-source';

import {User} from './user';
import {UsersRepositoryService} from './users-repository.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
})
export class ListComponent implements OnInit
{
    public displayedColumns: Array<string> = ['email'];

    public dataSource: PaginatedDataTableSource<User>;

    @ViewChild(MatPaginator)
    public paginator: MatPaginator;

    @ViewChild(MatSort)
    public sort: MatSort;

    constructor(
        private users: UsersRepositoryService,
    ) {}

    public ngOnInit(): void
    {
        this.dataSource = new PaginatedDataTableSource(this.paginator, this.sort, (pageIndex: number, pageSize: number, sort: Sort) => {
            return this.users.getAll(pageIndex, pageSize, sort);
        });
    }

    public reload(): void
    {
        this.dataSource.reload();
    }
}
```
